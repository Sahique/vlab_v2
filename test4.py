import docker
import pymysql
import socket
import mount
import traceback

def find_free_port(start_port, end_port):
    for port in range(start_port, end_port + 1):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.settimeout(1)  # Adjust the timeout based on your needs

        result = sock.connect_ex(('139.14.11.48', port))
        #result = sock.connect_ex(('127.0.0.1', port))
        if result != 0:
            # Port is available
            print("port no>> ",str(port))
            return port

    raise Exception("No available ports ")



def startdocker(dockerData):
    response=[]
    # Initialize the Docker client
    print("starting docker ---- ",dockerData)
    client = docker.from_env()

    # id, creator_id, course_name, lab_type, start_date, stop_date, no_of_workbench, status, buffer_start_datime, buffer_stop_datime
    #  25, 2, ECE, phy, 2023-11-27T00:27:00.000Z, 2023-11-27T01:00:00.000Z, 5, active, 2023-11-22 17:15:00, 2023-11-24 17:45:00
    try:
        
        #db = pymysql.connect(host='db', user='root',password='root',db='vlab',cursorclass=pymysql.cursors.DictCursor)
        #mycursor = db.cursor()
        #print("DB connected")
        #response={"Ack": "failure"}
        #sql_qry= "SELECT * FROM vlab.labtype WHERE labName=%s " 
        #mycursor.execute(sql_qry,dockerData[3])
        #labTypes =  mycursor.fetchall()
        #print(labTypes) ;  
        

        # id, labName, imageName, description, size, imagePath
        list=[{'id':'3', 
          'labName':'AIML', 
          'imageName':'linux OS', 
          'description':'linux os with tools for AIML ', 
          'size':'2.3', 
          'imagePath':'sreedocker123/ubuntu_xfce_novnc_apcog_ai_lab_gergmany:latest'}]

        # Specify the image to use for the container
        image_name = "sreedocker123/ubuntu_xfce_novnc_apcog_ai_lab_gergmany:latest"
        #sql_qry="SELECT * FROM vlab.workbenchallocation WHERE bookingId=%s "
        #print((sql_qry,dockerData[0]))
        #mycursor.execute(sql_qry,dockerData[0])
        #list =  mycursor.fetchall()
        #print(list);  



        # Define the number of containers to create
        #num_containers = 0
        
        # Loop to create and start multiple containers
        #for i in range(num_containers):
        ports=[]
        startPort=6930 ; endPort=6999
        for _ in range(len(list)):
            port = find_free_port(startPort, endPort)
            print("Selected port:", port); ports.append(port)
            startPort = port + 1 ;
        print(ports)
        i=0
        for x in list:
            #if (x["participantId"]!="none"):
            # Define the container directory path where the data will be available inside the container
            container_name = x["workbenchId"]
            host_port=6901
            container_port= ports[i]; i+=1;
            container_settings = {
                "image": image_name,
                "detach": True,
                "name": container_name,
                "ports": { f"{host_port}/tcp": container_port},
                "environment":{"VNC_PW": ""},
                "privileged": True
                # You can specify more container settings here as needed
            }
            try:
                container = client.containers.create(**container_settings)
                print(f"Container {container_name} created with ID: {container.id}")

                # Start the container
                container.start()

                
                #sql_qry= "UPDATE vlab.workbenchallocation SET link=%s WHERE workbenchId=%s "
                #print(str(allocation_data['bookingId']+str(x)),str(x),"none",allocation_data['bookingId'])
            
                #values=("http://139.14.11.48:"+str(container_port)+"/vnc.html",x["workbenchId"])
                #mycursor.execute(sql_qry,values)
                #db.commit()
                #print(mycursor.rowcount, "was inserted.")
                print("docker created at : "+"http://139.14.11.48:"+str(container_port)+"/vnc.html")


            except docker.errors.ImageNotFound:
                print(f"Image '{image_name}' not found.")
            except docker.errors.APIError as e:
                print(f"Docker API error: {e}")
        
    except Exception as e:
        #response["Ack"] = "exception: " + str(e)
        print("Exception: "+str(e))
        traceback.print_exc()
    
    #db.close()
    return response

startdocker((39, '24', '1664', 'AIML', '2023-12-04T01:32:00.000Z', '2023-12-04T01:42:00.000Z', '2', 'active', '2023-12-04 01:23:00', '2023-12-04 01:00:00'))

#sudo docker run -it -d --privileged --name my_test_container -p 6940:6901 -v /home/urz/User40:/home/Workspace -e VNC_PW= sreedocker123/ubuntu_xfce_novnc_apcog_ai_lab_gergmany:latest
