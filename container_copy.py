import docker
import pymysql


def startdocker(dockerData):
    response=[]
    # Initialize the Docker client
    print(dockerData)
    client = docker.from_env()

    # id, creator_id, course_name, lab_type, start_date, stop_date, no_of_workbench, status, buffer_start_datime, buffer_stop_datime
    #  25, 2, ECE, phy, 2023-11-27T00:27:00.000Z, 2023-11-27T01:00:00.000Z, 5, active, 2023-11-22 17:15:00, 2023-11-24 17:45:00
    try:
        db = pymysql.connect(host='localhost', user='root',password='root',db='vlab',cursorclass=pymysql.cursors.DictCursor)
        mycursor = db.cursor()
        print("DB connected")
        response={"Ack": "failure"}
        mycursor = db.cursor()
        sql_qry= "SELECT * FROM vlab.labtype WHERE labName=%s " 
        mycursor.execute(sql_qry,dockerData[3])
        labTypes =  mycursor.fetchall()
        print(labTypes) ;   mycursor.close()

        # Specify the image to use for the container
        image_name = "docker/welcome-to-docker" # dockerData[3]

        # Define the host directory path you want to mount into the container
        host_directory_path = "C:/Users/Sahique/Documents/New folder/"

        # Define the container directory path where the data will be available inside the container
        #container_directory_path = "/path/in/container"

        sql_qry="SELECT * FROM vlab.workbenchallocation WHERE bookingId=%s "
        mycursor.execute(sql_qry,dockerData[0])
        list =  mycursor.fetchall()
        print(list);  



        # Define the number of containers to create
        #num_containers = 0

        # Loop to create and start multiple containers
        #for i in range(num_containers):
        for x in list:
            if (x[3]!="none"):
                # Define the container directory path where the data will be available inside the container
                container_directory_path = "/home"
                container_name = f"my_container_{x[1]}"
                host_directory_path = x[5]
                host_port=6901
                container_port=6901
                container_settings = {
                    "image": image_name,
                    "detach": True,
                    "name": container_name,
                    "volumes": {
                        host_directory_path: {
                            "bind": container_directory_path,
                            "mode": "rw"  # Read-write mode, use "ro" for read-only
                        }
                    },
                    "ports": {
                        f"{host_port}/tcp": container_port
                    }
                    # You can specify more container settings here as needed
                }

                try:
                    container = client.containers.create(**container_settings)
                    print(f"Container {container_name} created with ID: {container.id}")

                    # Start the container
                    container.start()

                    sql_qry= "UPDATE vlab.workbenchallocation SET containerid=%s WHERE workbenchId=%s "
                    #print(str(allocation_data['bookingId']+str(x)),str(x),"none",allocation_data['bookingId'])
                
                    values=(container.id,x[1])
                    mycursor.execute(sql_qry,values)
                    db.commit()
                    print(mycursor.rowcount, "was inserted.")
                    if(mycursor.rowcount>0):    response.append(f"Participant {x[3]} assigned with container ID: {container.id}")

                except docker.errors.ImageNotFound:
                    print(f"Image '{image_name}' not found.")
                except docker.errors.APIError as e:
                    print(f"Docker API error: {e}")
    except:
        response['Ack']="DB not connected"
        print("DB not connected")
    
    db.close()
    return response

def delete_container(container_id):
    response=""
    try:
        client = docker.from_env()
        container = client.containers.get(container_id)
        container.remove(force=True)  # Force removal if the container is running
        print(f"Container {container_id} has been deleted.")
        response=f"Container {container_id} has been deleted."
    except docker.errors.NotFound as e:
        print(f"Container {container_id} not found: {e}")
        response=f"Container {container_id} has been deleted."
    except docker.errors.APIError as e:
        print(f"An error occurred while deleting the container: {e}")
        response=f"Container {container_id} has been deleted."
    return response

# Replace 'your_container_id' with the actual ID of the container you want to delete
#container_id = 'af9839f273c55e58535655aceb5aafd429624d5a67ec558e8c597019bc8504c9'

#delete_container(container_id)








'''
import docker as docker_module

# Initialize the Docker client
client = docker_module.from_env()

# Define container configurations
containers = [
    {"name": "container1", "image": "your-docker-image:tag"},
    {"name": "container2", "image": "your-docker-image:tag"},
    # Add more container configurations as needed
]

# Create and start Docker containers
for container_config in containers:
    container_name = container_config["name"]
    container_image = container_config["image"]
    
    # Check if the container already exists, and remove it if it does
    try:
        existing_container = client.containers.get(container_name)
        existing_container.remove(force=True)
    except docker_module.errors.NotFound:
        pass
    
    # Create and start the container
    container = client.containers.run(
        image=container_image,
        name=container_name,
        detach=True,  # Run the container in the background
        # Add more container options as needed
    )
    
    print(f"Container {container_name} started with ID: {container.id}")
'''
