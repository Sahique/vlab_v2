-- MySQL dump 10.13  Distrib 8.0.28, for Win64 (x86_64)
--
-- Host: localhost    Database: vlab
-- ------------------------------------------------------
-- Server version	8.0.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `container_list`
--

DROP TABLE IF EXISTS `container_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `container_list` (
  `container_id` int NOT NULL,
  `container_type` varchar(45) DEFAULT NULL,
  `container_name` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`container_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `container_list`
--

LOCK TABLES `container_list` WRITE;
/*!40000 ALTER TABLE `container_list` DISABLE KEYS */;
/*!40000 ALTER TABLE `container_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `container_schedular`
--

DROP TABLE IF EXISTS `container_schedular`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `container_schedular` (
  `id` int NOT NULL AUTO_INCREMENT,
  `creator_id` varchar(45) DEFAULT NULL,
  `course_name` varchar(45) DEFAULT NULL,
  `lab_type` varchar(45) DEFAULT NULL,
  `start_date` varchar(45) DEFAULT NULL,
  `stop_date` varchar(45) DEFAULT NULL,
  `no_of_workbench` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `buffer_start_datime` varchar(45) DEFAULT NULL,
  `buffer_stop_datime` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `container_schedular`
--

LOCK TABLES `container_schedular` WRITE;
/*!40000 ALTER TABLE `container_schedular` DISABLE KEYS */;
INSERT INTO `container_schedular` VALUES (1,'John mathew','Embedded Systems','Micro Controller lab','2023-09-28T09:00:32.955Z','2023-10-28T21:00:32.955Z','15','inactive',NULL,NULL),(2,'Ana mendis','Embedded Systems','HDL lab','2023-09-28T10:00:32.955Z','2023-10-28T11:00:32.955Z','4','inactive',NULL,NULL),(6,'Mohammed Sahique','AIM','cloud computing','2023-10-04T15:00:00.000Z','2023-10-04T16:00:00.000Z','10','inactive',NULL,NULL),(13,'2','Embedded Systems','Micro Controller lab','2023-09-28T17:49:32.955Z','2023-10-28T17:49:32.955Z','12','inactive',NULL,NULL),(14,'John mathew','Embedded Systems','Micro Controller lab','2023-10-22T17:00:00.955Z','2023-10-28T17:00:00.955Z','15','inactive',NULL,NULL),(15,'sahiq','Embedded Systems','DSP','2023-10-20T14:00:00.955Z','2023-10-25T15:00:00.955Z','10','inactive','2023-10-20 14:15:00.955000','2023-10-25 14:45:00.955000'),(18,'Ramkumar','AIML','AIML','2023-11-10T10:00:00.000Z','2023-11-20T13:00:00.000Z','20','inactive','2023-11-10 10:15:00','2023-11-20 12:45:00'),(19,'sahique','chemistry','compound labs','2023-11-10T11:00:00.000Z','2023-11-20T13:00:00.000Z','20','inactive','2023-11-10 11:15:00','2023-11-20 12:45:00'),(20,'sahique HS','biology','zoology','2023-11-10T12:00:00.000Z','2023-11-20T13:00:00.000Z','20','inactive','2023-11-10 12:15:00','2023-11-20 12:45:00'),(21,'Bert','maths','AI','2023-11-05T17:00:00.000Z','2023-11-20T18:00:00.000Z','10','inactive','2023-11-05 17:15:00','2023-11-20 17:45:00'),(22,'Cedric Bauer','VLAB Test room','demo lab','2023-10-25T12:00:00.000Z','2023-10-28T13:05:00.000Z','2','inactive','2023-10-25 12:15:00','2023-10-28 12:50:00'),(23,'Cedric Bauer','demo lab','demo lab','2023-11-09T13:40:00.000Z','2023-11-09T14:40:00.000Z','2','inactive','2023-11-25 12:15:00','2023-11-28 12:50:00'),(24,'2','demo lab','demo lab','2023-12-25T12:00:00.000Z','2023-12-28T13:05:00.000Z','2','inactive','2023-12-25 12:15:00','2023-12-28 12:50:00'),(25,'2','ECE','phy','2023-11-23T01:47:00.000Z','2023-11-24T01:00:00.000Z','5','active','2023-11-22 17:15:00','2023-11-24 17:45:00');
/*!40000 ALTER TABLE `container_schedular` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `labtype`
--

DROP TABLE IF EXISTS `labtype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `labtype` (
  `id` int NOT NULL AUTO_INCREMENT,
  `labName` varchar(45) DEFAULT NULL,
  `imageName` varchar(100) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `size` varchar(45) DEFAULT NULL,
  `imagePath` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `labtype`
--

LOCK TABLES `labtype` WRITE;
/*!40000 ALTER TABLE `labtype` DISABLE KEYS */;
INSERT INTO `labtype` VALUES (1,'demo lab','helloworld','This is a demo lab.','0.1',NULL),(2,'Micro Controller lab','mico_controller','lab for microcontroller only','0.5',NULL),(3,'AIML','linux OS','linux os with tools for AIML ','2.3','sreedocker123/ubuntu_xfce_novnc_apcog_ai_lab_gergmany:latest'),(5,'physics','physics_v1','demo image details','1','msahique/physics_v1');
/*!40000 ALTER TABLE `labtype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log`
--

DROP TABLE IF EXISTS `log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `log` (
  `sno` int NOT NULL AUTO_INCREMENT,
  `container_id` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`sno`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log`
--

LOCK TABLES `log` WRITE;
/*!40000 ALTER TABLE `log` DISABLE KEYS */;
/*!40000 ALTER TABLE `log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `registration`
--

DROP TABLE IF EXISTS `registration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `registration` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `phone` varchar(13) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `role` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `registration`
--

LOCK TABLES `registration` WRITE;
/*!40000 ALTER TABLE `registration` DISABLE KEYS */;
INSERT INTO `registration` VALUES (1,'sahique','msahique@gmail.com','8880655639','123456','admin'),(2,'teacher','teacher@gmail.com','9787564123','123456','teacher');
/*!40000 ALTER TABLE `registration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resources`
--

DROP TABLE IF EXISTS `resources`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `resources` (
  `Sno` int NOT NULL,
  `coreallocated` int DEFAULT NULL,
  `corebalance` int DEFAULT NULL,
  `totalcores` int DEFAULT NULL,
  `storageallocated` int DEFAULT NULL,
  `stroragebalance` int DEFAULT NULL,
  `totalstroage` int DEFAULT NULL,
  `ramallocated` int DEFAULT NULL,
  `rambalance` int DEFAULT NULL,
  `totalram` int DEFAULT NULL,
  PRIMARY KEY (`Sno`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='								';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resources`
--

LOCK TABLES `resources` WRITE;
/*!40000 ALTER TABLE `resources` DISABLE KEYS */;
INSERT INTO `resources` VALUES (1,0,0,200,0,100000,100000,NULL,NULL,NULL),(2,0,0,200,0,100000,100000,0,64,64),(3,0,200,200,0,100000,100000,0,64,64);
/*!40000 ALTER TABLE `resources` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `workbenchallocation`
--

DROP TABLE IF EXISTS `workbenchallocation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `workbenchallocation` (
  `sno` int NOT NULL AUTO_INCREMENT,
  `workbenchId` varchar(60) DEFAULT NULL,
  `bookingId` int DEFAULT NULL,
  `participantId` varchar(45) DEFAULT NULL,
  `containerid` varchar(80) DEFAULT NULL,
  `directorypath` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`sno`)
) ENGINE=InnoDB AUTO_INCREMENT=172 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `workbenchallocation`
--

LOCK TABLES `workbenchallocation` WRITE;
/*!40000 ALTER TABLE `workbenchallocation` DISABLE KEYS */;
INSERT INTO `workbenchallocation` VALUES (1,'0',5,'none',NULL,NULL),(2,'0',5,'none',NULL,NULL),(3,'0',5,'none',NULL,NULL),(4,'0',5,'none',NULL,NULL),(5,'0',5,'none',NULL,NULL),(6,'0',5,'none',NULL,NULL),(7,'0',5,'sahique','',''),(8,'0',5,'none',NULL,NULL),(9,'0',5,'none',NULL,NULL),(10,'0',5,'none',NULL,NULL),(11,'0',6,'none',NULL,NULL),(12,'0',6,'none',NULL,NULL),(13,'0',6,'none',NULL,NULL),(14,'0',6,'none',NULL,NULL),(15,'0',6,'none',NULL,NULL),(16,'0',6,'none',NULL,NULL),(17,'0',6,'none',NULL,NULL),(18,'0',6,'none',NULL,NULL),(19,'0',6,'none',NULL,NULL),(20,'0',6,'none',NULL,NULL),(21,'12_0',12,'45',NULL,NULL),(22,'12_1',12,'12',NULL,NULL),(23,'12_2',12,'none',NULL,NULL),(24,'12_3',12,'none',NULL,NULL),(25,'12_4',12,'none',NULL,NULL),(26,'13_0',13,'none',NULL,NULL),(27,'13_1',13,'none',NULL,NULL),(28,'13_2',13,'none',NULL,NULL),(29,'13_3',13,'none',NULL,NULL),(30,'13_4',13,'none',NULL,NULL),(31,'13_5',13,'none',NULL,NULL),(32,'13_6',13,'none',NULL,NULL),(33,'13_7',13,'none',NULL,NULL),(34,'13_8',13,'none',NULL,NULL),(35,'13_9',13,'none',NULL,NULL),(36,'13_10',13,'none',NULL,NULL),(37,'13_11',13,'none',NULL,NULL),(38,'13_12',13,'none',NULL,NULL),(39,'13_13',13,'none',NULL,NULL),(40,'13_14',13,'none',NULL,NULL),(41,'14_0',14,'none',NULL,NULL),(42,'14_1',14,'none',NULL,NULL),(43,'14_2',14,'none',NULL,NULL),(44,'14_3',14,'none',NULL,NULL),(45,'14_4',14,'none',NULL,NULL),(46,'14_5',14,'none',NULL,NULL),(47,'14_6',14,'none',NULL,NULL),(48,'14_7',14,'none',NULL,NULL),(49,'14_8',14,'none',NULL,NULL),(50,'14_9',14,'none',NULL,NULL),(51,'14_10',14,'none',NULL,NULL),(52,'14_11',14,'none',NULL,NULL),(53,'14_12',14,'none',NULL,NULL),(54,'14_13',14,'none',NULL,NULL),(55,'14_14',14,'none',NULL,NULL),(56,'15_0',15,'none',NULL,NULL),(57,'15_1',15,'none',NULL,NULL),(58,'15_2',15,'none',NULL,NULL),(59,'15_3',15,'none',NULL,NULL),(60,'15_4',15,'none',NULL,NULL),(61,'15_5',15,'none',NULL,NULL),(62,'15_6',15,'none',NULL,NULL),(63,'15_7',15,'none',NULL,NULL),(64,'15_8',15,'none',NULL,NULL),(65,'15_9',15,'none',NULL,NULL),(96,'18_0',18,'none',NULL,NULL),(97,'18_1',18,'none',NULL,NULL),(98,'18_2',18,'none',NULL,NULL),(99,'18_3',18,'none',NULL,NULL),(100,'18_4',18,'none',NULL,NULL),(101,'18_5',18,'none',NULL,NULL),(102,'18_6',18,'none',NULL,NULL),(103,'18_7',18,'none',NULL,NULL),(104,'18_8',18,'none',NULL,NULL),(105,'18_9',18,'none',NULL,NULL),(106,'18_10',18,'none',NULL,NULL),(107,'18_11',18,'none',NULL,NULL),(108,'18_12',18,'none',NULL,NULL),(109,'18_13',18,'none',NULL,NULL),(110,'18_14',18,'none',NULL,NULL),(111,'18_15',18,'none',NULL,NULL),(112,'18_16',18,'none',NULL,NULL),(113,'18_17',18,'none',NULL,NULL),(114,'18_18',18,'none',NULL,NULL),(115,'18_19',18,'none',NULL,NULL),(116,'19_0',19,'none',NULL,NULL),(117,'19_1',19,'none',NULL,NULL),(118,'19_2',19,'none',NULL,NULL),(119,'19_3',19,'none',NULL,NULL),(120,'19_4',19,'none',NULL,NULL),(121,'19_5',19,'none',NULL,NULL),(122,'19_6',19,'none',NULL,NULL),(123,'19_7',19,'none',NULL,NULL),(124,'19_8',19,'none',NULL,NULL),(125,'19_9',19,'none',NULL,NULL),(126,'19_10',19,'none',NULL,NULL),(127,'19_11',19,'none',NULL,NULL),(128,'19_12',19,'none',NULL,NULL),(129,'19_13',19,'none',NULL,NULL),(130,'19_14',19,'none',NULL,NULL),(131,'19_15',19,'none',NULL,NULL),(132,'19_16',19,'none',NULL,NULL),(133,'19_17',19,'none',NULL,NULL),(134,'19_18',19,'none',NULL,NULL),(135,'19_19',19,'none',NULL,NULL),(136,'20_0',20,'none',NULL,NULL),(137,'20_1',20,'none',NULL,NULL),(138,'20_2',20,'none',NULL,NULL),(139,'20_3',20,'none',NULL,NULL),(140,'20_4',20,'none',NULL,NULL),(141,'20_5',20,'none',NULL,NULL),(142,'20_6',20,'none',NULL,NULL),(143,'20_7',20,'none',NULL,NULL),(144,'20_8',20,'none',NULL,NULL),(145,'20_9',20,'none',NULL,NULL),(146,'20_10',20,'none',NULL,NULL),(147,'20_11',20,'none',NULL,NULL),(148,'20_12',20,'none',NULL,NULL),(149,'20_13',20,'none',NULL,NULL),(150,'20_14',20,'none',NULL,NULL),(151,'20_15',20,'none',NULL,NULL),(152,'20_16',20,'none',NULL,NULL),(153,'20_17',20,'none',NULL,NULL),(154,'20_18',20,'none',NULL,NULL),(155,'20_19',20,'none',NULL,NULL),(156,'21_0',21,'none',NULL,NULL),(157,'21_1',21,'none',NULL,NULL),(158,'21_2',21,'none',NULL,NULL),(159,'21_3',21,'none',NULL,NULL),(160,'21_4',21,'none',NULL,NULL),(161,'21_5',21,'none',NULL,NULL),(162,'21_6',21,'none',NULL,NULL),(163,'21_7',21,'none',NULL,NULL),(164,'21_8',21,'none',NULL,NULL),(165,'21_9',21,'none',NULL,NULL),(166,'22_0',22,'none',NULL,NULL),(167,'22_1',22,'none',NULL,NULL),(168,'23_0',23,'none',NULL,NULL),(169,'23_1',23,'none',NULL,NULL),(170,'24_0',24,'none',NULL,NULL),(171,'24_1',24,'85648','','');
/*!40000 ALTER TABLE `workbenchallocation` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-11-25 18:35:33
