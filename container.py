import docker
import pymysql
import socket
import mount
import traceback


def find_free_port(start_port, end_port):
    for port in range(start_port, end_port + 1):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.settimeout(1)  # Adjust the timeout based on your needs

        #result = sock.connect_ex(('139.14.11.48', port))
        result = sock.connect_ex(('127.0.0.1', port))
        if result != 0:
            # Port is available
            print("port no>> ",str(port))
            return port

    raise Exception("No available ports ")


# dockerData= (course_id,imagepath,id,port)
def startdocker(dockerData):
    response=[]
    # Initialize the Docker client
    print("starting docker ---- ",dockerData)
    client = docker.from_env()

    # id, creator_id, course_name, lab_type, start_date, stop_date, no_of_workbench, status, buffer_start_datime, buffer_stop_datime
    #  25, 2, ECE, phy, 2023-11-27T00:27:00.000Z, 2023-11-27T01:00:00.000Z, 5, active, 2023-11-22 17:15:00, 2023-11-24 17:45:00
    try:
        
        db = pymysql.connect(host='db', user='root',password='root',db='vlab',cursorclass=pymysql.cursors.DictCursor)
        mycursor = db.cursor()
        print("DB connected")
        response={"Ack": "failure"}
        ports=[]
        startPort=6930 ; endPort=6999
       
        port = find_free_port(startPort, endPort)
        print("Selected port:", port); ports.append(port)    
        print(ports)
        
        
        container_name = dockerData['course_id']+"_"+dockerData['id']
        host_port=6901
        container_port= port; 
        container_settings = {
            "image": dockerData['image_name'],
            "detach": True,
            "name": container_name,
            "ports": { f"{host_port}/tcp": container_port},
            "environment":{"VNC_PW": ""},
            "privileged": True,
            "volumes": {"/home/urz/User" + dockerData['id']: {'bind': '/home/Workspace', 'mode': 'rw'}}

            # You can specify more container settings here as needed
        }
        try:
            container = client.containers.create(**container_settings)
            print(f"Container {container_name} created with ID: {container.id}")

            # Start the container
            container.start()
            url="http://139.14.11.48:"+str(container_port)+"/vnc.html"
            sql_qry= "INSERT INTO vlab.workbenchallocation (courseName, participantId,status,workbenchId,link) VALUES (%s,%s,%s,%s,%s) " 
            values=(dockerData['course_id'],dockerData['id'],"active",container_name,url,)
            mycursor.execute(sql_qry,values)
            #
            db.commit()
            print(str(mycursor.rowcount), "was inserted.")
            print(mycursor.rowcount, "was inserted.")
            if(mycursor.rowcount>0): response['Ack']= "workbenche allocated"; response['link']=url

        except docker.errors.ImageNotFound:
            print(f"Image '{dockerData['image_name']}' not found.")
        except docker.errors.APIError as e:
            print(f"Docker API error: {e}")
    
    except Exception as e:
        #response["Ack"] = "exception: " + str(e)
        print("Exception: "+str(e))
        traceback.print_exc()
    
    db.close()
    return response






def startdocker_old(dockerData):
    response=[]
    # Initialize the Docker client
    print("starting docker ---- ",dockerData)
    client = docker.from_env()

    # id, creator_id, course_name, lab_type, start_date, stop_date, no_of_workbench, status, buffer_start_datime, buffer_stop_datime
    #  25, 2, ECE, phy, 2023-11-27T00:27:00.000Z, 2023-11-27T01:00:00.000Z, 5, active, 2023-11-22 17:15:00, 2023-11-24 17:45:00
    try:
        
        db = pymysql.connect(host='db', user='root',password='root',db='vlab',cursorclass=pymysql.cursors.DictCursor)
        mycursor = db.cursor()
        print("DB connected")
        response={"Ack": "failure"}
        sql_qry= "SELECT * FROM vlab.labtype WHERE labName=%s " 
        mycursor.execute(sql_qry,dockerData[3])
        labTypes =  mycursor.fetchall()
        print(labTypes) ;  
        

        # Specify the image to use for the container
        image_name = labTypes[0]["imagePath"]
        sql_qry="SELECT * FROM vlab.workbenchallocation WHERE bookingId=%s "
        print((sql_qry,dockerData[0]))
        mycursor.execute(sql_qry,dockerData[0])
        list =  mycursor.fetchall()
        print(list);  



        # Define the number of containers to create
        #num_containers = 0
        
        # Loop to create and start multiple containers
        #for i in range(num_containers):
        ports=[]
        startPort=6930 ; endPort=6999
        for _ in range(len(list)):
            port = find_free_port(startPort, endPort)
            print("Selected port:", port); ports.append(port)
            startPort = port + 1 ;
        print(ports)
        i=0
        for x in list:
            #if (x["participantId"]!="none"):
            # Define the container directory path where the data will be available inside the container
            container_name = x["workbenchId"]
            host_port=6901
            container_port= ports[i]; i+=1;
            container_settings = {
                "image": image_name,
                "detach": True,
                "name": container_name,
                "ports": { f"{host_port}/tcp": container_port},
                "environment":{"VNC_PW": ""},
                "privileged": True
                # You can specify more container settings here as needed
            }
            try:
                container = client.containers.create(**container_settings)
                print(f"Container {container_name} created with ID: {container.id}")

                # Start the container
                container.start()

                
                sql_qry= "UPDATE vlab.workbenchallocation SET link=%s WHERE workbenchId=%s "
                #print(str(allocation_data['bookingId']+str(x)),str(x),"none",allocation_data['bookingId'])
            
                values=("http://139.14.11.48:"+str(container_port)+"/vnc.html",x["workbenchId"])
                mycursor.execute(sql_qry,values)
                db.commit()
                print(mycursor.rowcount, "was inserted.")
                print("docker created")


            except docker.errors.ImageNotFound:
                print(f"Image '{image_name}' not found.")
            except docker.errors.APIError as e:
                print(f"Docker API error: {e}")
        
    except Exception as e:
        #response["Ack"] = "exception: " + str(e)
        print("Exception: "+str(e))
        traceback.print_exc()
    
    db.close()
    return response

def delete_container(container_id):
    response=""
    #print("-->>",container_id,container_id["workbenchId"])
    try:
        client = docker.from_env()
        container = client.containers.get(container_id["workbenchId"])
        mount.umount_nfs_volume(container_id["workbenchId"], "139.14.11.59", "mnt/shared/"+container_id["participantId"], "/home/headless")
        container.remove(force=True)  # Force removal if the container is running
        print("Container " +container_id["workbenchId"]+" has been deleted.")
        response="Container " +container_id["workbenchId"]+" has been deleted."
    except docker.errors.NotFound as e:
        print(container_id["workbenchId"]+" not found: "+ e)
        response=container_id["workbenchId"]+" not found: "+ e
    except docker.errors.APIError as e:
        print("An error occurred while deleting the container: "+e)
        response="An error occurred while deleting the container: "+e
    return response

# Replace 'your_container_id' with the actual ID of the container you want to delete
#container_id = 'af9839f273c55e58535655aceb5aafd429624d5a67ec558e8c597019bc8504c9'

#delete_container(container_id)

#startdocker((39, '24', '1664', 'AIML', '2023-12-04T01:32:00.000Z', '2023-12-04T01:42:00.000Z', '2', 'active', '2023-12-04 01:23:00', '2023-12-04 01:00:00'))

#startdocker((31, '8', '2000', 'AIML', '2023-12-04T20:50:00.000Z', '2023-12-04T20:58:00.000Z', '2', 'active', '2023-12-04 01:23:00', '2023-12-04 01:00:00'))


data= {"course_id":"","imagepath":"sahiqaimlgermanylab","id":"40"}
startdocker(data)



'''
import docker as docker_module

# Initialize the Docker client
client = docker_module.from_env()

# Define container configurations
containers = [
    {"name": "container1", "image": "your-docker-image:tag"},
    {"name": "container2", "image": "your-docker-image:tag"},
    # Add more container configurations as needed
]

# Create and start Docker containers
for container_config in containers:
    container_name = container_config["name"]
    container_image = container_config["image"]
    
    # Check if the container already exists, and remove it if it does
    try:
        existing_container = client.containers.get(container_name)
        existing_container.remove(force=True)
    except docker_module.errors.NotFound:
        pass
    
    # Create and start the container
    container = client.containers.run(
        image=container_image,
        name=container_name,
        detach=True,  # Run the container in the background
        # Add more container options as needed
    )
    
    print(f"Container {container_name} started with ID: {container.id}")
'''
