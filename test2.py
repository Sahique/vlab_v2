import mysql.connector

# Replace these values with your MySQL server details
host = '129.154.44.165'
user = 'root'
password = ''
database = 'dummy'

# Establish a connection to the MySQL server
try:
    connection = mysql.connector.connect(
        host=host,
        user=user,
        password=password,
        database=database
    )

    if connection.is_connected():
        print(f"Connected to MySQL server at {host}")

        # Perform database operations here

except mysql.connector.Error as err:
    print(f"Error: {err}")

finally:
    # Close the database connection
    if 'connection' in locals() and connection.is_connected():
        connection.close()
        print("Connection closed")
