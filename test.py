import docker


client = docker.from_env()
image_name = "sreedocker123/ubuntu_xfce_novnc_apcog_ai_lab_gergmany:latest" # dockerData[3]

# Define the host directory path you want to mount into the container
#host_directory_path = "C:/Users/Sahique/Documents/New folder/"

# Define the container directory path where the data will be available inside the container
#container_directory_path = "/path/in/container"
#container_directory_path = "/home"
container_name = f"my_test_container"
host_port=6901
container_port=6940
container_settings = {
    "image": image_name,
    "detach": True,
    "name": container_name,
    "ports": {
        f"{host_port}/tcp": container_port
    }
# You can specify more container settings here as needed
}
try:
    container = client.containers.create(**container_settings)
    print(f"Container {container_name} created with ID: {container.id}")

    # Start the container
    container.start()


except docker.errors.ImageNotFound:
    print(f"Image '{image_name}' not found.")
except docker.errors.APIError as e:
    print(f"Docker API error: {e}")

'''
{
    "Creator id": "26046749",
    "Course Name": "1082128528399",
    "Lab Type": "physics",
    "Number of workbench": "2",
    "Start Date-Time": "2023-12-02T10:10:00.000Z",
    "End Date-Time": "2023-12-02T12:12:00.000Z"
}
'''