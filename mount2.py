import subprocess
import docker

import sys
print(sys.executable)


def mount_nfs_volume(container_name, nfs_server, nfs_path, local_mount_path):
    # Get the container ID by name
    container_id = subprocess.check_output(['docker', 'ps', '-q', '-f', "name="+container_name]).decode('utf-8').strip()
    
    # Run the mount command inside the container
    mount_command = "mount -t nfs" +nfs_server+":"+nfs_path+" "+local_mount_path+ " -o nolock"
    subprocess.run(['docker', 'exec', container_id, 'sh', '-c', mount_command])

if __name__ == "__main__":
    # Docker container name
    container_name = "User_test"

    # NFS server details
    nfs_server = "139.14.11.59"
    nfs_path = "/mnt/share"

    # Local mount path inside the container
    local_mount_path = "/home/headless/data"

    # Mount NFS volume into the Docker container
    mount_nfs_volume(container_name, nfs_server, nfs_path, local_mount_path)
