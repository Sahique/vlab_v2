from datetime import datetime, timedelta


# Given timestamp: -1 day, 23:58:27.147124
given_timestamp = datetime.now() - timedelta(days=0, hours=0, minutes=15, seconds=00, microseconds=000000)

# Current time
current_time = datetime.now()

# Calculate the time difference
time_difference = current_time - given_timestamp

print(f"Given Timestamp: {given_timestamp}")
print(f"Current Time: {current_time}")
print(f"Time Difference: {time_difference}")

start_time_str = "2023-12-04T04:50:00.000Z"
# Correct the format by adding a colon between hours and minutes
start_time_str = start_time_str.replace("T", " ").replace("Z", "")

# Parse the corrected start time string into a datetime object
start_time = datetime.strptime(start_time_str, "%Y-%m-%d %H:%M:%S.%f")
print(datetime.now(),"----",start_time)
time_difference = datetime.now() - start_time
print(time_difference);


'''
import datetime


start_time_str = "2023-12-04T04:40:00.000Z"
# Correct the format by adding a colon between hours and minutes
start_time_str = start_time_str.replace("T", " ").replace("Z", "")

# Parse the corrected start time string into a datetime object
start_time = datetime.datetime.strptime(start_time_str, "%Y-%m-%d %H:%M:%S.%f")

# Get the current time
#print(datetime.datetime.utcnow(), datetime.now())
#time_difference = start_time - (datetime.datetime.utcnow()).astimezone(timezone('Asia/Kolkata'))

print(datetime.datetime.now(),"----",start_time)
time_difference = datetime.datetime.now() - start_time
#print(time_difference);

days = time_difference.days
hours, remainder = divmod(time_difference.seconds, 3600)
minutes, _ = divmod(remainder, 60)

# Print the result
print(f"Days: {days}, Hours: {hours}, Minutes: {minutes}")


# Check if the time difference is 15 minutes or less
print(time_difference,":",datetime.timedelta(minutes=15),"--",time_difference,":",datetime.timedelta())
if time_difference <= datetime.timedelta(minutes=15) and time_difference >= datetime.timedelta():
    print("ok")

'''