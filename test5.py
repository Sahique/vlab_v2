import subprocess
import docker

import sys
print(sys.executable)


def mount_nfs_volume(container_name, nfs_server, nfs_path, local_mount_path):
    # Get the container ID by name
    response=""
    try:
        container_id = subprocess.check_output(['docker', 'ps', '-q', '-f', f'name={container_name}']).decode('utf-8').strip()
        # Run the mount command inside the container
        # mount -t nfs 139.14.11.59:/mnt/share /home/headless/data -o nolock
        mount_command = f'mount -t nfs -o rw,sync,nolock {nfs_server}:{nfs_path} {local_mount_path}'
        subprocess.run(['docker', 'exec', container_id, 'sh', '-c', mount_command])
        response="success"
    except Exception as e:
        response="exception: ",str(e)
    return response

def umount_nfs_volume(container_name, nfs_server, nfs_path, local_mount_path):
    response=""
    try:
        # Get the container ID by name
        container_id = subprocess.check_output(['docker', 'ps', '-q', '-f', f'name={container_name}']).decode('utf-8').strip()
        # Run the mount command inside the container
        mount_command = f'umount -t nfs {local_mount_path} '
        subprocess.run(['docker', 'exec', container_id, 'sh', '-c', mount_command])
        response="success"
    except Exception as e:
        response="exception: ",str(e)
    return response

#val=mount_nfs_volume("2000_31_0", "139.14.11.59", "mnt/shared/user_1", "/home/workspace")
val=umount_nfs_volume("2000_31_0", "139.14.11.59", "mnt/shared/user_1", "/home/workspace")
print(val)